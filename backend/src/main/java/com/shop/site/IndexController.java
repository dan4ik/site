package com.shop.site;

import com.shop.site.domain.Product;
import com.shop.site.domain.SiteUser;
import com.shop.site.model.AuthenticationRequest;
import com.shop.site.model.AuthenticationResponse;
import com.shop.site.model.MessageResponse;
import com.shop.site.model.RegistrationRequest;
import com.shop.site.repository.RepositorySiteUser;
import com.shop.site.service.MyUserDetailsService;

import com.shop.site.utils.JwtUtil;
import jdk.nashorn.internal.ir.RuntimeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;


import java.util.HashMap;
import java.util.Map;


@RestController
public class IndexController {

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private MyUserDetailsService userDetailsService;

    @Autowired
    RepositorySiteUser repositorySiteUser;

    @Autowired
    private JwtUtil JwtTokenUtil;

    @RequestMapping("/")
    public String helloWorld(){
        return "Hello World!!";
    }

    @CrossOrigin(origins = "http://localhost:8080")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception
    {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword())
            );
        } catch (BadCredentialsException e){
            throw new Exception("Incorrect username or password", e);
        }
        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        final String jwt = JwtTokenUtil.generateToken(userDetails);
        return  ResponseEntity.ok(new AuthenticationResponse(jwt, userDetails));
    }


    @CrossOrigin(origins = "http://localhost:8080")
    @PostMapping("/registration")
    public ResponseEntity<?> registerUser(@RequestBody RegistrationRequest registrationRequest) {
        if (repositorySiteUser.existsByName(registrationRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        if (repositorySiteUser.existsByEmail(registrationRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }

        // Create new user's account
        SiteUser user = new SiteUser(   registrationRequest.getUsername(),
                                        registrationRequest.getLogin(),
                                        registrationRequest.getEmail(),
                                        encoder.encode(registrationRequest.getPassword()));

//        Set<String> strRoles = registrationRequest.getRole();
//        Set<Role> roles = new HashSet<>();
//
//        if (strRoles == null) {
//            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
//                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
//            roles.add(userRole);
//        } else {
//            strRoles.forEach(role -> {
//                switch (role) {
//                    case "admin":
//                        Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
//                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
//                        roles.add(adminRole);
//
//                        break;
//                    case "mod":
//                        Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
//                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
//                        roles.add(modRole);
//
//                        break;
//                    default:
//                        Role userRole = roleRepository.findByName(ERole.ROLE_USER)
//                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
//                        roles.add(userRole);
//                }
//            });
//        }

        user.setRole(registrationRequest.getRole());
        repositorySiteUser.save(user);

        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }

}