
//import repositoryProduct from '../../repository/RepositoryProductLocal.js';
// import babelPolyfill from 'babel-polyfill'
import Vue from 'vue'
import repositoryProduct from '../../repository/RepositoryProductLocal.js';
//import AuthService from "../../services/auth.service";
export default {
  namespaced: true,
  state: {
    productsBasket: [],
    isLoading: false,
    countItem: 0
  },
  getters:{
    product(state){
      return state.productsBasket;
    },
    isLoading(state){
      return state.isLoading;
    },
    getCountBasket(state){
      return state.countItem;
    }
  }
  ,
  mutations: {
    CREATING(state) {
      state.isLoading = true;
      state.error = null;
    },
    fetch_products(state, products){
      state.productsBasket = products;
      // state.isLoading = false;
    },
    checkoutSuccess(state){
      state.productsBasket = [];
      state.countItem = state.productsBasket.length;
      localStorage.removeItem('itemBasketProduct');
    }
  },
  actions: {

    addToBasket({state},  itemadd ){
      if(state.productsBasket.includes(itemadd)){
        state.productsBasket.forEach(function (item, i) {
          if (item.id === itemadd.id){
            if(state.productsBasket[i].quantity !== 0){
              state.productsBasket[i].countbasket += 1;
              state.productsBasket[i].quantity -= 1;
            }
          }
        });
      }else{
        Vue.set(itemadd, 'countbasket', 1);
        state.productsBasket.push(itemadd);
        itemadd.quantity -= 1;
      }
      state.countItem = state.productsBasket.length;

      console.log("setlocalstorage");
      if (state.productsBasket.length > 0) {
        try{
          localStorage.setItem("itemBasketProduct", JSON.stringify(state.productsBasket));
          console.log("set local storage");
          console.log(JSON.stringify(state.productsBasket));
        }catch (e) {
          console.log("error localstorage set itemproduct");
          console.log(e.message);
          localStorage.removeItem('itemBasketProduct');
        }
      }
    },
    getItemFromlocaSt({commit, state}){
      if (localStorage.getItem('itemBasketProduct')) {
        console.log("getlocalstorage");
        try {
          console.log(JSON.parse(localStorage.getItem('itemBasketProduct')));
          commit('fetch_products', JSON.parse(localStorage.getItem('itemBasketProduct')));
          state.countItem = state.productsBasket.length;
          //this.itemBasketProduct = JSON.parse(localStorage.getItem('itemBasketProduct'));

        } catch(e) {
          console.log("Error local storage");
          console.log(e.message);
          localStorage.removeItem('itemBasketProduct');
        }
      }
    },
    deleteFromBasket({ state}, itemdel){
      state.productsBasket.forEach(function (item, i) {
        if (item.id === itemdel.id){
          if(state.productsBasket[i].countbasket > 1){
            state.productsBasket[i].countbasket -= 1;
            state.productsBasket[i].quantity += 1;
          }else{
            state.productsBasket.splice(i, 1);
          }
        }
      });
      state.countItem = state.productsBasket.length;
    },
    checkout({state, commit}){
      return repositoryProduct.checkout(state.productsBasket).then(
          response => {
            commit('checkoutSuccess');
            return Promise.resolve(response.data);
          },
          error => {
            commit('checkoutFailure');
            return Promise.reject(error);
          }
      );
      //после успеха ощистить корзину
      //локал сторадже
      //сообщение об успешном создании заказа
    }
  }
};

