package com.shop.site.repository;

import java.util.Optional;
import com.shop.site.domain.SiteUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepositorySiteUser extends JpaRepository <SiteUser, String>{
    Optional<SiteUser> findByName(String username);

    Boolean existsByName(String username);

    Boolean existsByEmail(String email);
}
