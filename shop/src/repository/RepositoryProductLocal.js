/**
 * Created by dserov on 09.12.2019.
 */
import axios from "axios";
import API_URL from "../conf/API_URL";
import authHeader from '../services/auth-header';
// const token = localStorage.getItem('token');
// axios.defaults.headers = {'Authorization': `Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJmb28iLCJleHAiOjE1ODk4Mjg5NjYsImlhdCI6MTU4OTc5Mjk2Nn0.hiQfjp4GLWTH7Ir_5w0qxIwNd8VMyu4WitFnijc9VWU`, 'Content-Type': 'application/json'};

// if(token){
//axios.defaults.headers['Authorization'] = 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJkZW5pczc3NyIsImV4cCI6MTU5MDAwNjA2MywiaWF0IjoxNTg5OTcwMDYzfQ.42B2GqPlugvzd6yc4JL8uuZ1HIJbouv4WIkR8Q47Rws';

// }else{
//     axios.defaults.headers.common['Authorization'] = 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJmb28iLCJleHAiOjE1ODk4Mjg5NjYsImlhdCI6MTU4OTc5Mjk2Nn0.hiQfjp4GLWTH7Ir_5w0qxIwNd8VMyu4WitFnijc9VWU'
// }
// let Products = [{"id":1,"name":"Mazda","model":"Millenia","price":"37918","year of issue":"1995","image":"http://dummyimage.com/249x220.png/dddddd/000000","quantity":"11"},
//   {"id":2,"name":"Kia","model":"Sportage","price":"33882","year of issue":"1997","image":"http://dummyimage.com/249x220.bmp/dddddd/000000","quantity":"8"},
//   {"id":3,"name":"Chevrolet","model":"Traverse","price":"38228","year of issue":"2012","image":"http://dummyimage.com/249x220.jpg/dddddd/000000","quantity":"13"},
//   {"id":4,"name":"Isuzu","model":"Hombre","price":"30370","year of issue":"1997","image":"http://dummyimage.com/249x220.jpg/dddddd/000000","quantity":"12"},
//   {"id":5,"name":"Chevrolet","model":"Corvette","price":"36185","year of issue":"2006","image":"http://dummyimage.com/249x220.bmp/dddddd/000000","quantity":"12"},
//   {"id":6,"name":"Saturn","model":"Ion","price":"40889","year of issue":"2006","image":"http://dummyimage.com/249x220.jpg/dddddd/000000","quantity":"12"},
//   {"id":7,"name":"Ford","model":"Thunderbird","price":"33497","year of issue":"1984","image":"http://dummyimage.com/249x220.bmp/dddddd/000000","quantity":"11"},
//   {"id":8,"name":"Mercury","model":"Mariner","price":"25850","year of issue":"2009","image":"http://dummyimage.com/249x220.bmp/dddddd/000000","quantity":"24"},
//   {"id":9,"name":"Toyota","model":"RAV4","price":"40921","year of issue":"2011","image":"http://dummyimage.com/249x220.png/dddddd/000000","quantity":"7"},
//   {"id":10,"name":"Toyota","model":"Supra","price":"41427","year of issue":"1998","image":"http://dummyimage.com/249x220.png/dddddd/000000","quantity":"11"},
//   {"id":11,"name":"Dodge","model":"Shadow","price":"29946","year of issue":"1993","image":"http://dummyimage.com/249x220.bmp/dddddd/000000","quantity":"19"},
//   {"id":12,"name":"Oldsmobile","model":"Bravada","price":"41570","year of issue":"1997","image":"http://dummyimage.com/249x220.bmp/dddddd/000000","quantity":"3"},
//   {"id":13,"name":"Mitsubishi","model":"Challenger","price":"24108","year of issue":"2000","image":"http://dummyimage.com/249x220.jpg/dddddd/000000","quantity":"3"},
//   {"id":14,"name":"Cadillac","model":"CTS-V","price":"23849","year of issue":"2011","image":"http://dummyimage.com/249x220.png/dddddd/000000","quantity":"12"},
//   {"id":15,"name":"Pontiac","model":"Trans Sport","price":"25008","year of issue":"1991","image":"http://dummyimage.com/249x220.jpg/dddddd/000000","quantity":"8"},
//   {"id":16,"name":"Volkswagen","model":"Cabriolet","price":"26743","year of issue":"1987","image":"http://dummyimage.com/249x220.bmp/dddddd/000000","quantity":"3"},
//   {"id":17,"name":"Chrysler","model":"PT Cruiser","price":"16330","year of issue":"2010","image":"http://dummyimage.com/249x220.png/dddddd/000000","quantity":"1"},
//   {"id":18,"name":"GMC","model":"Yukon","price":"43610","year of issue":"2008","image":"http://dummyimage.com/249x220.bmp/dddddd/000000","quantity":"11"},
//   {"id":19,"name":"GMC","model":"Savana 2500","price":"43865","year of issue":"2004","image":"http://dummyimage.com/249x220.bmp/dddddd/000000","quantity":"1"},
//   {"id":20,"name":"Chevrolet","model":"Express 2500","price":"40707","year of issue":"2006","image":"http://dummyimage.com/249x220.bmp/dddddd/000000","quantity":"12"}];

export default {

    fetch() {
        axios.defaults.headers['Authorization'] = authHeader();
        // return Products;
        console.log("Запрос");
        // axios.defaults.headers.head['Authorization'] = 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJmb28iLCJleHAiOjE1ODk4Mjg5NjYsImlhdCI6MTU4OTc5Mjk2Nn0.hiQfjp4GLWTH7Ir_5w0qxIwNd8VMyu4WitFnijc9VWU';
        // axios.defaults.headers.head['Content-Type'] = 'application/json';
        console.log(axios.defaults.headers);
        return axios.get(API_URL + "products");
    },
    add(data){
        axios.defaults.headers['Authorization'] = authHeader();
        console.log("ASYNC edit");
        console.log(data.item);
        return axios.post(API_URL + "products/add/", data.item);
    },
    edit(data){
        axios.defaults.headers['Authorization'] = authHeader();
        console.log("ASYNC edit");
        console.log(typeof data.item);
        return axios.post( API_URL + "products/edit/", data.item);
    },
    delete(data){
        axios.defaults.headers['Authorization'] = authHeader();
        console.log("ASYNC delete");
        console.log(data);
        return axios.post(API_URL + "products/delete/", data.item);
    },
    checkout(data) {
        //логика по оформлению заказа
        axios.defaults.headers['Authorization'] = authHeader();
        console.log("ASYNC checkout");
        console.log(data);
        return axios.post(API_URL + "checkout", data);
    }
};