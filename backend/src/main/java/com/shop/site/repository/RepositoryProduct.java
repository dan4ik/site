package com.shop.site.repository;

import com.shop.site.domain.Product;
import org.springframework.data.repository.CrudRepository;

public interface RepositoryProduct extends CrudRepository<Product, Integer> {
}
