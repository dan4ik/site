package com.shop.site;

import com.shop.site.domain.Order;
import com.shop.site.domain.Product;
import com.shop.site.model.AuthenticationRequest;
import com.shop.site.model.AuthenticationResponse;
import com.shop.site.model.MessageResponse;
import com.shop.site.repository.RepositoryOrder;
import com.shop.site.utils.uuidShort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import com.shop.site.repository.RepositoryProduct;
import org.springframework.security.core.context.SecurityContextHolder;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@RestController
public class ProductController {
    @Autowired
    private RepositoryProduct productRepository;
    @Autowired
    private RepositoryOrder repositoryOrder;

    @CrossOrigin(origins = "http://localhost:8080")
    @RequestMapping("/products")
    public @ResponseBody Iterable<Product> getProducts(){
        return productRepository.findAll();
    }

    @CrossOrigin(origins = "http://localhost:8080")
    @RequestMapping(path = "/products/add/", method= RequestMethod.POST)
    public @ResponseBody Product addProducts(@RequestBody Product product){
        productRepository.save(product);
        return product;
    }

    @CrossOrigin(origins = "http://localhost:8080")
    @RequestMapping(path="/products/edit/", method= RequestMethod.POST)
    //@ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody Product editProduct(@RequestBody Product product){
        productRepository.save(product);
        return product;
    }

    @CrossOrigin(origins = "http://localhost:8080")
    @RequestMapping(path = "/products/delete/", method= RequestMethod.POST)
    public  @ResponseBody Product deleteProduct(@RequestBody Product product){
        productRepository.delete(product);
        return product;
    }

    @CrossOrigin(origins = "http://localhost:8080")
    @RequestMapping(path = "/checkout", method= RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody Order[] entities) throws Exception
    {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String numorder = uuidShort.shortUUID();

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        String curData = formatter.format(date);

        for (int i = 0; i < entities.length; i++) {
            System.out.println("Test array ");
            System.out.println(entities[i].getName() + " "+
                    entities[i].getModel()+ " "+
                    entities[i].getPrice()+ " "+
                    entities[i].getQuantity()+ " "+
                    entities[i].getYear()+ " "+
                    entities[i].getImage());
            entities[i].setUsername(((UserDetails)principal).getUsername());
            entities[i].setNumorder(numorder);
            entities[i].setDateorder(curData);
            repositoryOrder.save(entities[i]);
        }

        return  ResponseEntity.ok(new MessageResponse("Create orders successfully!"));
    }

}

