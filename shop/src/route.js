/**
 * Created by dserov on 07.12.2019.
 */

import Vue from 'vue'
import VueRouter from 'vue-router'


import Main from './components/Pages/Main.vue'
import Basket from './components/Basket.vue'
import Admin from './components/Pages/Admin.vue'
import Login from './components/Pages/Login.vue'
import Regist from "./components/Pages/Regist";

Vue.use(VueRouter);

const routes = [
  { path: '/', component: Main },
  { path: '/basket', component: Basket },
  { path: '/admin', component: Admin},
  { path: '/login', component: Login},
  { path: '/regist', component: Regist}
];

export default new VueRouter({
  routes
})
