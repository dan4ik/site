export default function authHeader() {
    console.log(localStorage.getItem('user'));
    let user = JSON.parse(localStorage.getItem('user'));

    if (user && user.jwt) {
        return  'Bearer ' + user.jwt ;
    } else {
        return '';
    }
}
