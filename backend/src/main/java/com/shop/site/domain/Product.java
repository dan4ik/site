package com.shop.site.domain;

import javax.persistence.*;

@Entity
public class Product {
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    @Id
    @SequenceGenerator(name="seq", initialValue=19, allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,  generator="seq")
    private Integer id;
    private String name;
    private String model;
    private String price;
    private String year;
    private String image;
    private String quantity;
}
