package com.shop.site.domain;


import javax.persistence.*;

@Entity
@Table(name="orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String numorder;
    private String dateorder;
    private String username;
    private String name;
    private String model;
    private String price;
    private String quantity;
    private String year;
    private String image;

    public Order() {
    }

    public Order(String numorder, String dateorder, String username, String name, String model, String price, String quantity, String year, String image ) {
        this.numorder = numorder;
        this.dateorder = dateorder;
        this.username = username;
        this.name = name;
        this.model = model;
        this.price = price;
        this.quantity = quantity;
        this.year = year;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumorder() {
        return numorder;
    }

    public void setNumorder(String numorder) {
        this.numorder = numorder;
    }

    public String getDateorder() {
        return dateorder;
    }

    public void setDateorder(String dateorder) {
        this.dateorder = dateorder;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
