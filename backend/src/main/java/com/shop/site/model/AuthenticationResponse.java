package com.shop.site.model;

import org.springframework.security.core.userdetails.UserDetails;

public class AuthenticationResponse {
    private final String jwt;

    private UserDetails userDetails;

    public AuthenticationResponse(String jwt, UserDetails userDetails) {
        this.jwt = jwt;
        this.userDetails = userDetails;
    }

    public String getJwt() {
        return jwt;
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }
}
