import axios from 'axios';

import API_URL from "../conf/API_URL";
// import authHeader from "./auth-header";

//axios.defaults.headers['Content-Type'] = "application/json";

class AuthService {
    login(user) {
        console.log("user send");
        console.log(typeof user);
        console.log(user);
        return axios
            .post(API_URL + 'login', user)
            .then(response => {
                if (response.data.jwt) {
                    localStorage.setItem('user', JSON.stringify(response.data));
                }
                return response.data;
            });
    }

    logout() {
        localStorage.removeItem('user');
    }

    register(user) {
        return axios.post(API_URL + 'registration', user);
    }
}

export default new AuthService();
