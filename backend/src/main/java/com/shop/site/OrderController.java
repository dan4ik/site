package com.shop.site;

import com.shop.site.domain.Order;
import com.shop.site.domain.Product;
import com.shop.site.repository.RepositoryOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class OrderController {

    @Autowired
    private RepositoryOrder repositoryOrder;


    @CrossOrigin(origins = "http://localhost:8080")
    @RequestMapping("/orders")
    public @ResponseBody
    Iterable<Order> getOrders(@RequestBody String userName){
        System.out.println("Test get users: " + userName);
        return repositoryOrder.findByusername(userName);
    }
}
