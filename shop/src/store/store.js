/**
 * Created by dserov on 09.12.2019.
 */
import Vue from 'vue';
import Vuex from 'vuex';

import storeProduct from './Product/storeProduct.js'
import storeBasket from './Basket/storeBasket.js'
import { auth } from './Auth/storeAuth.js'



Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    storeProduct: storeProduct,
    storeBasket: storeBasket,
    auth
  }
})