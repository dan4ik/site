package com.shop.site.service;

import com.shop.site.domain.SiteUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Objects;

public class SiteUserDetails implements UserDetails {

    private static final long serialVersionUID = 1L;

    private int id;

    private String name;

    private String login;

    private String email;

    private String password;

    private String role;

    public SiteUserDetails(int id, String name, String login, String email, String password,
                           String role) {
        this.id = id;
        this.name = name;
        this.login = login;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    public static SiteUserDetails build(SiteUser siteuser) {
//        List<GrantedAuthority> authorities = siteuser.getRole().stream()
//                .map(role -> new SimpleGrantedAuthority(role.getName().name()))
//                .collect(Collectors.toList());

        return new SiteUserDetails(
                siteuser.getId(),
                siteuser.getName(),
                siteuser.getLogin(),
                siteuser.getEmail(),
                siteuser.getPassword(),
                siteuser.getRole()
        );
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return name;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        SiteUserDetails user = (SiteUserDetails) o;
        return Objects.equals(id, user.id);
    }
}
