package com.shop.site.repository;

import com.shop.site.domain.Order;
import com.shop.site.domain.SiteUser;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface RepositoryOrder extends CrudRepository<Order, Integer> {
    Iterable<Order> findByusername(String username);
}
