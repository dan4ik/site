package com.shop.site.service;

import com.shop.site.domain.SiteUser;
import com.shop.site.repository.RepositorySiteUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    RepositorySiteUser repositorySiteUser;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        SiteUser user = repositorySiteUser.findByName(login)
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + login));
        System.out.println("Test get users: " + user.getLogin() + " " + user.getRole());
        return SiteUserDetails.build(user);
//        return new User("foo", "foo", new ArrayList<>());
    }
}
