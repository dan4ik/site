/**
 * Created by dserov on 09.12.2019.
 */


/**
 * Created by dserov on 26.11.2019.
 */
/**
 * Created by dserov on 29.08.2019.
 */

import repositoryProduct from '../../repository/RepositoryProductLocal.js';
// import babelPolyfill from 'babel-polyfill'

export default {
  namespaced: true,
  state: {
    products: {},
    isLoading: false,
    test: ''
  },
  getters:{
    product(state){
      return state.products;
    },
    isLoading(state){
      return state.isLoading;
    }
    // ,
    // isError(state){
    //   return state.error;
    // },
    // errorMsg(state){
    //   return state.errormsg;
    // },
    // isUpdate(state){
    //   return state.isUpdate;
    // }
  }
  ,
  mutations: {
    CREATING(state) {
      state.isLoading = true;
      state.error = null;
    },
    fetch_products(state, products) {
      state.products = products;
      state.isLoading = false;
      // state.error = null;
    },
     test(state){
       state.test = "0";
     }
    // ENDUPDATE(state){
    //   state.isUpdate = false;
    //   state.error = null;
    // },
    // fetch_errors_journalevg(state, error){
    //   state.isLoading = false;
    //   state.isUpdate = false;
    //   state.error = true;
    //   state.errormsg = error;
    //   state.journalevg = [];
    // },
    // setControlrec( state,  data  ) {
    //
    // }
  },
  actions: {
    async getProducts( { commit } ){
      commit('CREATING');
      try {
        //сервис по получению продуктов
        //два сервиса один локальной
        //другой удалеенно
        let response = await repositoryProduct.fetch();
        commit('fetch_products', response);
        console.log("RESPONE DATA journalUser");
        //return response.data;
      } catch (error) {
        console.log(error);
        console.log(error.response);
        console.log(error.status);
        // commit('fetch_errors_journalevg', error);
        //return null;
      }
    },
    async itemEdit( {commit}, data){
      //происходит обновление
      //вернуть ответ
      commit('test');
      console.log("ASYNC edit store");
      console.log(data.item);
      let response = await repositoryProduct.edit( data );
      return response;
    },
    async itemDelete( {commit, state}, data){
      commit('test');
      let response = await repositoryProduct.delete( data );
      state.products.data.forEach((currentValue, index)=>{
        if(currentValue.id === response.data.id){
          state.products.data.splice(index, 1);
        }
      });
      console.log("ASYNC delete");
      console.log(data.item);
      return response;

    },
    async itemAdd( {commit, state}, data){
      commit('test');
      let response = await repositoryProduct.add( data );
      console.log("ASYNC add reponse");
      state.products.data.push(response.data);
      console.log(state.products.data);
      console.log(response.data);
      return  response;
    }
  }
};


