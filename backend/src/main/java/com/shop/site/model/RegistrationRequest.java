package com.shop.site.model;

public class RegistrationRequest {
    private String username;
    private String password;
    private String login;
    private String email;
    private String role;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }



    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    //need default constructor for JSON Parsing
    public RegistrationRequest()
    {

    }

    public RegistrationRequest(String username, String password, String role, String login, String email) {
        this.setUsername(username);
        this.setPassword(password);
        this.setRole(role);
        this.setLogin(login);
        this.setEmail(email);
    }
}
